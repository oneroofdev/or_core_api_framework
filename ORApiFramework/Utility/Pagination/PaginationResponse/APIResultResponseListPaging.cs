﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ORApiFramework.HTTP.Response;
using ORApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.ModelBinding;

namespace ORApiFramework.Utility.Pagination
{
    public class APIResultResponseListPaging
    {
        public bool success { get; set; }
        public string code { get; set; }
        public string key { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public string transactionId { get; set; }
        public string transactionDateTime { get; set; }
        public List<object> items { get; set; }
        public List<object> filters { get; set; }
        public object otherItems { get; set; }
        public PaginationResponse pagination { get; set; }
        public PageInfoReponse pageInfo { get; set; }
        private PageManager pageMaster = new PageManager();

        public APIResultResponseListPaging()
        {
            code = "200";
            message = "Success";
            description = "";
            items = new List<object>();
            filters = new List<object>();
            pagination = new PaginationResponse();
            pageInfo = new PageInfoReponse();
            transactionId = Guid.NewGuid().ToString();
            transactionDateTime = DateTime.Now.ToString();

        }


        public HttpResponseMessage ResponseResult(HttpRequestMessage request, List<object> itemListResponse, ModelStateDictionary modelState, PaginationRequest pagination, object model, string ModuleName, string methodName)
        {

            HttpResponseMessage response;
            if (modelState.IsValid)
            {
                this.success = true;
                var itemList = pageMaster.getResultList(itemListResponse, pagination.limitPerPage, pagination.currentPage);
                this.items = itemList;
                this.pagination = pageMaster.getPagination(itemListResponse.Count(), pagination.limitPerPage, pagination.currentPage);
                this.pageInfo = pageMaster.getPageInfo(itemList.Count(), itemListResponse.Count());

                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.OK, StatusCode.OK.Value(), this);
                //Save_log.SaveLogObject(this, JObject.Parse(JsonConvert.SerializeObject(model)), ModuleName, methodName);//save log output

            }
            else
            {
                var erorMessage = ErrorMessageManagement.ErrorMessageUtil.getErorModelState(modelState);
                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.EROR, erorMessage, this);
                //Save_log.SaveLogObject(this, JObject.Parse(JsonConvert.SerializeObject(model)), ModuleName, methodName);

            }
            return response;
        }

    }

    public class FilterItem
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
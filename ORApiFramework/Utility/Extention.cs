﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORApiFramework
{ 
    public static class Extention
    {
       

        /// <summary>
        /// Geta an integer with specified source.
        /// </summary>
        /// <param name="source">
        /// The string number.
        /// </param>
        /// <returns>
        /// Integer number.
        /// </returns>
        public static int ToInt32(this string source)
        {
            int result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToInt32(source);

            return result;
        }

        /// <summary>
        /// Geta an decimal with specified source.
        /// </summary>
        /// <param name="source">
        /// The string decimal.
        /// </param>
        /// <returns>
        /// decimal number.
        /// </returns>
        public static decimal? ToDecimalDB(this string source)
        {
            decimal? result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToDecimal(source);

            return result;
        }

        public static decimal ToDecimal(this string source)
        {
            decimal result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToDecimal(source);

            return result;
        }

        /// <summary>
        /// Geta an decimal with specified source.
        /// </summary>
        /// <param name="source">
        /// The string decimal.
        /// </param>
        /// <returns>
        /// decimal number.
        /// </returns>
        public static bool IsNullOrEmptyWhiteSpace(this string source)
        {
            bool result = false;
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
                result = true;

            return result;
        }

        /// <summary>
        /// Geta an DateTime with specified source.
        /// </summary>
        /// <param name="source">
        /// The string DateTime.
        /// </param>
        /// <returns>
        /// DateTime number.
        /// </returns>
        public static DateTime? ToDbDateTime(this string source)
        {
            DateTime? result = null;
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
                result = Convert.ToDateTime(source);

            return result;
        }

        /// <summary>
        /// Geta an DateTime with specified source.
        /// </summary>
        /// <param name="source">
        /// The string DateTime.
        /// </param>
        /// <returns>
        /// DateTime number.
        /// </returns>
        public static DateTime ToDateTime(this string source)
        {
            DateTime result = DateTime.Now;
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
                result = Convert.ToDateTime(source);

            return result;
        }

    }
}
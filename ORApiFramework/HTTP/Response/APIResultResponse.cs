﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORApiFramework.HTTP.Response
{
    public class APIResultResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public string key { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public string transactionId { get; set; }
        public string transactionDateTime { get; set; }
        public object items { get; set; }
        public object otherItems { get; set; }



        public APIResultResponse()
        {
            code = "200";
            message = "Success";
            description = "";
            items = new object();
            transactionId = Guid.NewGuid().ToString();
            transactionDateTime = DateTime.Now.ToString();
        }
    }
}

﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Serializers;
using Newtonsoft.Json;
using ORApiFramework.Utility;
using System;
using System.Web.Configuration;

namespace ORApiFramework.Security
{
    public static class JwtManagement
    {
        public static string GenerateJasonWebToken()
        {
            string result;
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            try
            {
                var secret = WebConfigurationManager.AppSettings["TOKEN_SECRET_KEY"].ToString();
                // SECOND , MINUTES , HOURS
                string timeExUnit = WebConfigurationManager.AppSettings["TIME_EXPIRE_UNIT"].ToString();
                int timeExQty = WebConfigurationManager.AppSettings["TIME_EXPIRE_QTY"].ToInt32();
                UserAccess userAccess = new UserAccess();

                switch (timeExUnit)
                {
                    case "SECOND":
                        userAccess.TokenDateTime = DateTimeOffset.UtcNow.AddSeconds(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "MINUTES":
                        userAccess.TokenDateTime = DateTimeOffset.UtcNow.AddMinutes(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "HOURS":
                        userAccess.TokenDateTime = DateTimeOffset.UtcNow.AddHours(timeExQty).ToUnixTimeSeconds();
                        break;

                }
                userAccess.UserName = userName;
                var jsonUserAccess = JsonConvert.SerializeObject(userAccess);
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                result = encoder.Encode(userAccess, secret);

            }
            catch (Exception e)
            {
                throw new Exception(e.ErrorException());
            }

            return result;
        }


    }
}
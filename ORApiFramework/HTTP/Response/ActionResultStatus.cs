﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORApiFramework.HTTP.Response
{
    public class ActionResultStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public object data { get; set; }
        public object filter { get; set; }
        public object otherData { get; set; }
        public string key { get; set; }

        public ActionResultStatus()
        {
            success = true;
            code = 200;
            message = "OK";
            key = "";
        }
    }
}
